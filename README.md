# delports

My FreeBSD port repository

### Installing

Type those commands to be able to use delports repo

	mkdir -p /usr/local/etc/pkg/repos

	fetch -o /usr/local/etc/pkg/repos/delports.conf http://codeberg.org/glowiak/delports/raw/branch/master/delports.conf

	pkg update
